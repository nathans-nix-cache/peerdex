#!/usr/bin/env bash

###
## This script replicates the same steps as ci, except for cargo audit
###

# Turn on the guard rails
set -exuo pipefail

## TODO use subshell magic to make a nice interface here

# Lint the formatting
nix build .#lints.format.peerdex -L
# Audit it
nix develop -c cargo audit
# Run clippy
nix build .#lints.clippy.peerdex -L
# Build it
nix build .#peerdex -L
# Test it
nix develop -c cargo nextest run
nix develop -c cargo test --doc
# Document it
nix build .#docs.peerdex.doc -L
