//! Error type for cryptographic operations

use snafu::Snafu;

use super::Protocol;

/// Error type for cryptographic operations
#[derive(Debug, Snafu)]
#[non_exhaustive]
#[snafu(visibility(pub(crate)))]
pub enum CryptographyError {
    /// Protocol Suite Mismatch
    #[snafu(display("Mismatched protocols, expected: {:?}, got: {:?}", expected, actual))]
    ProtocolMismatch {
        /// Actual Protocol
        actual: Protocol,
        /// Expected Protocol
        expected: Protocol,
    },
    /// Compared [`Hash`](super::Hash)s were not equal
    HashMismatch,
    /// [`IdentitySignature`](super::IdentitySignature) did not verify
    IdentitySignatureMismatch,
    /// The version of the protocol in use is unsafe
    #[snafu(display("Insecure protocol in use: {:?}", protocol))]
    UnsafeProtocol {
        /// Protocol version in use
        protocol: Protocol,
    },
}
