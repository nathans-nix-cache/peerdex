//! Types that can establish Identity

use redacted::{
    formatter::{FullHex, TruncHex},
    RedactContents, Redacted,
};
use xxhash_rust::xxh3;

use super::{
    error::{CryptographyError, IdentitySignatureMismatchSnafu, ProtocolMismatchSnafu},
    HasProtocol, Protocol,
};

/// Representation of a identity-establishing public key within the protocol
#[derive(Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, Debug)]
pub enum Identity {
    /// The dummy signature used for V0 of the protocol use keyed 128-bit XXH3 where the public key
    /// and private key are the same
    ///
    /// # <span style="color:red">**Warning**</span>
    ///
    /// This is not a secure cryptographic signature, this is provided for testing use only, _do not_ use
    /// this in production.
    V0(Redacted<[u8; 136], FullHex>),
    /// The signature scheme used for V1 of the protcol is a
    /// [ed25519](https://github.com/jedisct1/rust-ed25519-compact) public key
    ///
    /// This is encoded here as its raw bytes, which, for the purposes of not clogging logs, will
    /// only have the first 8 hex characters displayed in the debug format.
    V1(Redacted<[u8; 32], TruncHex<8>>),
}

impl HasProtocol for Identity {
    fn protocol(&self) -> Protocol {
        match self {
            Identity::V0(_) => Protocol::V0,
            Identity::V1(_) => Protocol::V1,
        }
    }
}

impl Identity {
    /// Validates an [`IdentitySignature`] against this [`Identity`]
    ///
    /// Returns `Ok(())` for a valid signature
    ///
    /// # Errors
    ///
    /// - `Err(CryptographyError::ProtocolMismatch)` if the [`Protocol`] of the [`Identity`] does
    ///   not match the protocol of the [`IdentitySignature`]
    /// - `Err(CryptographyError::IdentitySignatureMismatch)` if the signature fails verification
    pub fn validate(
        &self,
        signature: &IdentitySignature,
        message: impl AsRef<[u8]>,
    ) -> Result<(), CryptographyError> {
        match self {
            Identity::V0(pk) => {
                if let IdentitySignature::V0(_) = signature {
                    // Just re"sign" the data lol
                    let sk = IdentitySecret::V0(*pk);
                    let other_signature = sk
                        .sign(message.as_ref(), None)
                        .expect("Impossible to fail signature");
                    if &other_signature == signature {
                        Ok(())
                    } else {
                        IdentitySignatureMismatchSnafu.fail()
                    }
                } else {
                    ProtocolMismatchSnafu {
                        actual: signature.protocol(),
                        expected: Protocol::V0,
                    }
                    .fail()
                }
            }
            Identity::V1(pk) => {
                if let IdentitySignature::V1(signature) = signature {
                    // Extract the public key
                    let pk = ed25519_compact::PublicKey::new(*pk.as_ref());
                    // Extract the signature
                    let sig = ed25519_compact::Signature::new(*signature.as_ref());
                    // Validate the signature
                    if pk.verify(message, &sig).is_ok() {
                        Ok(())
                    } else {
                        IdentitySignatureMismatchSnafu.fail()
                    }
                } else {
                    ProtocolMismatchSnafu {
                        actual: signature.protocol(),
                        expected: Protocol::V1,
                    }
                    .fail()
                }
            }
        }
    }
}

/// Representation of the secret key associated with an [`Identity`]
#[derive(Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, Debug)]
pub enum IdentitySecret {
    /// 16 byte XXH3 "key"
    ///
    /// # <span style="color:red">**Warning**</span>
    ///
    /// This is not a secure cryptographic signature, this is provided for testing use only, _do not_ use
    /// this in production.
    V0(Redacted<[u8; 136], FullHex>),
    /// 64 byte ed25519 key pair
    ///
    /// This is encoded here as its raw bytes, which, for the purposes of not clogging logs, will
    /// only have the first 8 hex characters displayed in the debug format.
    V1(Redacted<[u8; 64], TruncHex<8>>),
}

impl HasProtocol for IdentitySecret {
    fn protocol(&self) -> Protocol {
        match self {
            IdentitySecret::V0(_) => Protocol::V0,
            IdentitySecret::V1(_) => Protocol::V1,
        }
    }
}

impl IdentitySecret {
    /// Gets the assocaited [`Identity`] for this secret
    pub fn identity(&self) -> Identity {
        match self {
            // The publick and "private" "keys" are the same for V0 of the protocol
            IdentitySecret::V0(x) => Identity::V0(*x),
            IdentitySecret::V1(x) => {
                // Unpack the key pair
                let kp = ed25519_compact::KeyPair::from_slice(x.as_ref())
                    .expect("This can only error if the slice is an incorrect size");
                // Get the public key bytes
                let bytes: [u8; 32] = *kp.pk;
                Identity::V1(Redacted::new(bytes))
            }
        }
    }

    /// Produces a signature against this [`Identity`] and packs it up as an [`IdentitySignature`]
    ///
    /// # Errors
    ///
    /// Will return `Err(CryptographyError::ProtocolMismatch)` if the provided noise does not match
    /// the [`Protocol`] of this [`Identity`]
    pub fn sign(
        &self,
        message: &[u8],
        noise: Option<IdentityNoise>,
    ) -> Result<IdentitySignature, CryptographyError> {
        match self {
            IdentitySecret::V0(x) => {
                // Simply "HMAC" it
                let bytes: [u8; 16] = xxh3::xxh3_128_with_secret(message, x.as_ref()).to_le_bytes();
                Ok(IdentitySignature::V0(Redacted::new(bytes)))
            }
            IdentitySecret::V1(x) => {
                // Unpack the key pair
                let kp = ed25519_compact::KeyPair::from_slice(x.as_ref())
                    .expect("This can only error if the slice is an incorrect size");
                // Produce the signature
                match noise {
                    Some(IdentityNoise::V1(x)) => {
                        let signature: [u8; 64] =
                            *kp.sk.sign(message, Some(ed25519_compact::Noise::new(*x)));
                        Ok(IdentitySignature::V1(Redacted::new(signature)))
                    }
                    Some(x) => ProtocolMismatchSnafu {
                        actual: x.protocol(),
                        expected: self.protocol(),
                    }
                    .fail(),
                    None => {
                        let signature: [u8; 64] = *kp.sk.sign(message, None);
                        Ok(IdentitySignature::V1(Redacted::new(signature)))
                    }
                }
            }
        }
    }
}

/// Representation of a signature produced by an [`Identity`]
#[derive(Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, Debug)]
pub enum IdentitySignature {
    /// 16 byte XXH3 "signature"
    ///
    /// # <span style="color:red">**Warning**</span>
    ///
    /// This is not a secure cryptographic signature, this is provided for testing use only, _do not_ use
    /// this in production.
    V0(Redacted<[u8; 16], FullHex>),
    /// 64 byte ed25519 signature
    ///
    /// This is encoded here as its raw bytes, which, for the purposes of not clogging logs, will
    /// only have the first 8 hex characters displayed in the debug format.
    V1(Redacted<[u8; 64], TruncHex<8>>),
}

impl HasProtocol for IdentitySignature {
    fn protocol(&self) -> Protocol {
        match self {
            IdentitySignature::V0(_) => Protocol::V0,
            IdentitySignature::V1(_) => Protocol::V1,
        }
    }
}

/// Representation of optional noise for a signature
#[derive(Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, Debug)]
pub enum IdentityNoise {
    /// Version 0 has no noise
    V0,
    /// Version 1 uses 16 bytes of noise
    V1(Redacted<[u8; 16], RedactContents>),
}

impl HasProtocol for IdentityNoise {
    fn protocol(&self) -> Protocol {
        match self {
            IdentityNoise::V0 => Protocol::V0,
            IdentityNoise::V1(_) => Protocol::V1,
        }
    }
}

#[cfg(test)]
mod tests {
    use bincode::{config::standard, decode_from_slice, encode_to_vec};
    use proptest::prelude::*;

    use super::*;

    proptest! {
        // Make sure that things survive a round trip through bincode
        #[test]
        fn round_trip_bincode(protocol in any::<Protocol>(), message in any::<Vec<u8>>(), seed in any::<Vec<u8>>()) {
            let identity_secret: IdentitySecret = protocol.generate_identity_from_seed(&seed);
            let identity: Identity = identity_secret.identity();
            let signature: IdentitySignature = identity_secret.sign(&message, None).unwrap();
            let noise: IdentityNoise = protocol.generate_identity_noise_from_seed(&seed);
            // do the encodes
            let id_sec_bytes = encode_to_vec(&identity_secret, standard()).unwrap();
            let id_bytes = encode_to_vec(&identity, standard()).unwrap();
            let sig_bytes = encode_to_vec(&signature, standard()).unwrap();
            let noise_bytes = encode_to_vec(&noise, standard()).unwrap();
            // Do the decodes
            let id_sec_dec: IdentitySecret = decode_from_slice(&id_sec_bytes, standard()).unwrap().0;
            let id_dec: Identity = decode_from_slice(&id_bytes, standard()).unwrap().0;
            let sig_dec: IdentitySignature = decode_from_slice(&sig_bytes, standard()).unwrap().0;
            let noise_dec: IdentityNoise = decode_from_slice(&noise_bytes, standard()).unwrap().0;
            // Assert equality
            assert_eq!(id_sec_dec, identity_secret);
            assert_eq!(id_dec, identity);
            assert_eq!(sig_dec, signature);
            assert_eq!(noise_dec, noise);
        }

        // Make sure a noiseless signature validates on the data that produced it
        #[test]
        fn noiseless_validation(protocol in any::<Protocol>(), message in any::<Vec<u8>>(), seed in any::<Vec<u8>>()) {
            let identity_secret: IdentitySecret = protocol.generate_identity_from_seed(&seed);
            let identity: Identity = identity_secret.identity();
            let signature: IdentitySignature = identity_secret.sign(&message, None).unwrap();
            assert!(identity.validate(&signature, &message).is_ok());
        }

        // Make sure a noiseful signature validates on the data that produced it
        #[test]
        fn noiseful_validation(
            protocol in any::<Protocol>(),
            message in any::<Vec<u8>>(),
            seed in any::<Vec<u8>>(),
            noise_seed in any::<Vec<u8>>(),
        ) {
            let identity_secret: IdentitySecret = protocol.generate_identity_from_seed(&seed);
            let identity: Identity = identity_secret.identity();
            let noise: IdentityNoise = protocol.generate_identity_noise_from_seed(&noise_seed);
            let signature: IdentitySignature = identity_secret.sign(&message, Some(noise)).unwrap();
            assert!(identity.validate(&signature, &message).is_ok());
        }

        // Make sure that two signatures of the same data in different protocols return the correct
        // error on verification
        #[test]
        fn non_equality_different_protocol(
            message in any::<Vec<u8>>(),
            seed in any::<Vec<u8>>(),
        ) {
            // Generate the keys and signature
            let id_sec_1 = Protocol::V0.generate_identity_from_seed(&seed);
            let id_1 = id_sec_1.identity();
            let signature_1 = id_sec_1.sign(&message, None).unwrap();
            let id_sec_2 = Protocol::V1.generate_identity_from_seed(&seed);
            let id_2 = id_sec_2.identity();
            let signature_2 = id_sec_2.sign(&message, None).unwrap();
            assert!(matches!(id_2.validate(&signature_1, &message), Err(CryptographyError::ProtocolMismatch { .. })));
            assert!(matches!(id_1.validate(&signature_2, &message), Err(CryptographyError::ProtocolMismatch { .. })));
        }

        // Make sure that two signatures with the same protocol and key but different data return
        // the correct error on verification
        #[test]
        fn same_key_different_data(
            protocol in any::<Protocol>(),
            (message_1, message_2) in any::<(Vec<u8>, Vec<u8>)>()
                .prop_filter("Values must not be equal",
                             |(a,b)| {a != b }),
            seed in any::<Vec<u8>>(),
        ) {
            let id_sec = protocol.generate_identity_from_seed(&seed);
            let id = id_sec.identity();
            let signature_1 = id_sec.sign(&message_1, None).unwrap();
            let signature_2 = id_sec.sign(&message_2, None).unwrap();
            // Assert that each signature validates against the correct message
            assert!(id.validate(&signature_1, &message_1).is_ok());
            assert!(id.validate(&signature_2, &message_2).is_ok());
            // Assert that each signature does not validate against the incorrect message
            assert!(matches!(id.validate(&signature_1, &message_2), Err(CryptographyError::IdentitySignatureMismatch)));
            assert!(matches!(id.validate(&signature_2, &message_1), Err(CryptographyError::IdentitySignatureMismatch)));
        }

        // Make sure that two signatures with the same protocol and data but different keys
        #[test]
        fn different_keys_same_data(
            protocol in any::<Protocol>(),
            (seed_1, seed_2) in any::<(Vec<u8>, Vec<u8>)>()
                .prop_filter("Values must not be equal",
                             |(a,b)| {a != b }),
            message in any::<Vec<u8>>(),
        ) {
            let id_sec_1 = protocol.generate_identity_from_seed(&seed_1);
            let id_1 = id_sec_1.identity();
            let id_sec_2 = protocol.generate_identity_from_seed(&seed_2);
            let id_2 = id_sec_2.identity();
            let signature_1 = id_sec_1.sign(&message, None).unwrap();
            let signature_2 = id_sec_2.sign(&message, None).unwrap();
            // Assert that the correct signatures validate against the correct keys
            assert!(id_1.validate(&signature_1, &message).is_ok());
            assert!(id_2.validate(&signature_2, &message).is_ok());
            // Assert that attempting to validate against an incorrect key returns an error
            assert!(matches!(id_1.validate(&signature_2, &message), Err(CryptographyError::IdentitySignatureMismatch)));
            assert!(matches!(id_2.validate(&signature_1, &message), Err(CryptographyError::IdentitySignatureMismatch)));
        }
    }
}
