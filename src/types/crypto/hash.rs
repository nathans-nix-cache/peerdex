//! Implementation for the [`enum@Hash`] enum

use redacted::{
    formatter::{FullHex, TruncHex},
    Redacted,
};

use super::{
    error::{HashMismatchSnafu, ProtocolMismatchSnafu},
    CryptographyError, HasProtocol, Protocol,
};

/// Representation of a hash within the protocol
#[derive(Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, Debug)]
pub enum Hash {
    /// The hash for V0 of the protocol is 128-bit XXH3
    ///
    /// # <span style="color:red">**Warning**</span>
    ///
    /// This is not a secure cryptographic hash, this is provided for testing use only, _do not_ use
    /// this in production.
    V0(Redacted<[u8; 16], FullHex>),
    /// The hash for V1 of the protocol is vanilla 32-byte output
    /// [BLAKE3](https://github.com/BLAKE3-team/BLAKE3)
    ///
    /// For the purposes of not clogging logs, the debug format will include the first 8 hex
    /// characters and truncate the rest of the hash.
    V1(Redacted<[u8; 32], TruncHex<8>>),
}

impl HasProtocol for Hash {
    fn protocol(&self) -> Protocol {
        match self {
            Hash::V1(_) => Protocol::V1,
            Hash::V0(_) => Protocol::V0,
        }
    }
}

impl Hash {
    /// Verifies this `Hash` against another `Hash`
    ///
    /// Will return `Ok(())` if the `Hash`s match
    ///
    /// # Errors
    ///
    /// - `Err(CryptograhyError::ProtocolMismatch)` if the two hashes have different protocol
    ///   versions
    /// - `Err(CryptograhyError::HashMismatch)` if the two hashes have the same protocol version,
    ///   but do not match
    pub fn equals(&self, other: &Hash) -> Result<(), CryptographyError> {
        match self {
            Hash::V1(hash) => {
                if let Hash::V1(other) = other {
                    // FIXME: This should be a constant time comparison
                    if hash == other {
                        Ok(())
                    } else {
                        HashMismatchSnafu.fail()
                    }
                } else {
                    ProtocolMismatchSnafu {
                        actual: other.protocol(),
                        expected: self.protocol(),
                    }
                    .fail()
                }
            }
            Hash::V0(hash) => {
                if let Hash::V0(other) = other {
                    if hash == other {
                        Ok(())
                    } else {
                        HashMismatchSnafu.fail()
                    }
                } else {
                    ProtocolMismatchSnafu {
                        actual: other.protocol(),
                        expected: self.protocol(),
                    }
                    .fail()
                }
            }
        }
    }
    /// Validates the provided data against this `Hash`
    ///
    /// Constructs a new hash using the provided data and the same [`Protocol`] as `self`, and
    /// performs a comparison, returning `Ok(())` if the resulting hash matches `self`.
    ///
    /// # Errors
    ///
    /// - `Err(CryptographyError::HashMismatch)` if the resulting hash does not match `self`
    pub fn validate(&self, data: impl AsRef<[u8]>) -> Result<(), CryptographyError> {
        let new_hash = self.protocol().hash(data);
        self.equals(&new_hash)
    }
}

#[cfg(test)]
mod tests {
    use bincode::{config::standard, decode_from_slice, encode_to_vec};
    use proptest::prelude::*;

    use super::*;

    proptest! {
        // Make sure a hash survives a round trip through bincode
        #[test]
        fn round_trip_bincode(protocol in any::<Protocol>(), bytes in any::<Vec<u8>>()) {
            let original = protocol.hash(&bytes);
            let buffer = encode_to_vec(&original, standard()).unwrap();
            let decoded = decode_from_slice(&buffer, standard()).unwrap().0;
            assert!(original.equals(&decoded).is_ok());
        }
        // Make sure a hash is equal to itself
        #[test]
        fn self_equality(protocol in any::<Protocol>(), bytes in any::<Vec<u8>>()) {
            let hash_1: Hash = protocol.hash(&bytes);
            assert!(hash_1.equals(&hash_1).is_ok());
            let hash_2: Hash = protocol.hash(&bytes);
            assert!(hash_2.equals(&hash_2).is_ok());
            assert!(hash_1.equals(&hash_2).is_ok());
            assert!(hash_1.validate(&bytes).is_ok());
        }
        // Make sure that two hases with with same protocol but different data are not equal
        #[test]
        fn non_equality_same_protocol(protocol in any::<Protocol>(),
                                      (bytes_1, bytes_2) in any::<(Vec<u8>, Vec<u8>)>()
                                        .prop_filter("Values must not be equal",
                                                     |(a,b)| {a != b })) {
            let hash_1 = protocol.hash(&bytes_1);
            let hash_2 = protocol.hash(&bytes_2);
            assert!(matches!(hash_1.equals(&hash_2), Err(CryptographyError::HashMismatch)));
        }
        // Make sure two hashes with different protocols but the same data return the correct error
        #[test]
        fn non_equality_different_protocol(bytes in any::<Vec<u8>>()) {
            let hash_1 = Protocol::V0.hash(&bytes);
            let hash_2 = Protocol::V1.hash(&bytes);
            assert!(matches!(hash_1.equals(&hash_2), Err(CryptographyError::ProtocolMismatch{..})));
        }
        // Make sure two arbitrary hashes with different data are non_equal
        #[test]
        fn non_equality((protocol_1, protocol_2) in any::<(Protocol, Protocol)>(),
                        (bytes_1, bytes_2) in any::<(Vec<u8>, Vec<u8>)>()
                                        .prop_filter("Values must not be equal",
                                                     |(a,b)| {a != b })) {
            let hash_1 = protocol_1.hash(bytes_1);
            let hash_2 = protocol_2.hash(bytes_2);
            assert!(hash_1.equals(&hash_2).is_err());
        }
    }
}
