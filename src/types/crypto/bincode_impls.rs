//! Bincode trait implementations for the crypto types
use bincode::{
    de::Decoder,
    enc::Encoder,
    error::{AllowedEnumVariants, DecodeError, EncodeError},
    Decode, Encode,
};
use redacted::Redacted;

use super::{Hash, Identity, IdentityNoise, IdentitySecret, IdentitySignature};

impl Encode for Hash {
    fn encode<E: Encoder>(&self, encoder: &mut E) -> Result<(), EncodeError> {
        match self {
            Self::V0(field_0) => {
                <u32 as Encode>::encode(&(0u32), encoder)?;
                <[u8; 16] as Encode>::encode(field_0, encoder)?;
                Ok(())
            }
            Hash::V1(field_0) => {
                <u32 as Encode>::encode(&(1u32), encoder)?;
                <[u8; 32] as Encode>::encode(field_0, encoder)?;
                Ok(())
            }
        }
    }
}

impl Decode for Hash {
    fn decode<D: Decoder>(decoder: &mut D) -> core::result::Result<Self, DecodeError> {
        let variant_index = <u32 as Decode>::decode(decoder)?;
        match variant_index {
            0u32 => {
                let inner = <[u8; 16] as Decode>::decode(decoder)?;
                Ok(Self::V0(Redacted::new(inner)))
            }
            1u32 => {
                let inner = <[u8; 32] as Decode>::decode(decoder)?;
                Ok(Self::V1(Redacted::new(inner)))
            }
            variant => Err(DecodeError::UnexpectedVariant {
                found: variant,
                type_name: "Hash",
                allowed: AllowedEnumVariants::Range { min: 0, max: 1 },
            }),
        }
    }
}

impl Encode for Identity {
    fn encode<E: Encoder>(&self, encoder: &mut E) -> Result<(), EncodeError> {
        match self {
            Identity::V0(x) => {
                <u32 as Encode>::encode(&(0_u32), encoder)?;
                <[u8; 136] as Encode>::encode(x, encoder)?;
                Ok(())
            }
            Identity::V1(x) => {
                <u32 as Encode>::encode(&(1_u32), encoder)?;
                <[u8; 32] as Encode>::encode(x, encoder)?;
                Ok(())
            }
        }
    }
}

impl Decode for Identity {
    fn decode<D: Decoder>(decoder: &mut D) -> Result<Self, DecodeError> {
        let variant_index = <u32 as Decode>::decode(decoder)?;
        match variant_index {
            0u32 => {
                let inner = <[u8; 136] as Decode>::decode(decoder)?;
                Ok(Self::V0(Redacted::new(inner)))
            }
            1u32 => {
                let inner = <[u8; 32] as Decode>::decode(decoder)?;
                Ok(Self::V1(Redacted::new(inner)))
            }
            variant => Err(DecodeError::UnexpectedVariant {
                found: variant,
                type_name: "Identity",
                allowed: AllowedEnumVariants::Range { min: 0, max: 1 },
            }),
        }
    }
}

impl Encode for IdentitySecret {
    fn encode<E: Encoder>(&self, encoder: &mut E) -> Result<(), EncodeError> {
        match self {
            IdentitySecret::V0(x) => {
                <u32 as Encode>::encode(&(0_u32), encoder)?;
                <[u8; 136] as Encode>::encode(x, encoder)?;
                Ok(())
            }
            IdentitySecret::V1(x) => {
                <u32 as Encode>::encode(&(1_u32), encoder)?;
                <[u8; 64] as Encode>::encode(x, encoder)?;
                Ok(())
            }
        }
    }
}

impl Decode for IdentitySecret {
    fn decode<D: Decoder>(decoder: &mut D) -> Result<Self, DecodeError> {
        let variant_index = <u32 as Decode>::decode(decoder)?;
        match variant_index {
            0u32 => {
                let inner = <[u8; 136] as Decode>::decode(decoder)?;
                Ok(Self::V0(Redacted::new(inner)))
            }
            1u32 => {
                let inner = <[u8; 64] as Decode>::decode(decoder)?;
                Ok(Self::V1(Redacted::new(inner)))
            }
            variant => Err(DecodeError::UnexpectedVariant {
                found: variant,
                type_name: "IdentitySecret",
                allowed: AllowedEnumVariants::Range { min: 0, max: 1 },
            }),
        }
    }
}

impl Encode for IdentitySignature {
    fn encode<E: Encoder>(&self, encoder: &mut E) -> Result<(), EncodeError> {
        match self {
            IdentitySignature::V0(x) => {
                <u32 as Encode>::encode(&(0_u32), encoder)?;
                <[u8; 16] as Encode>::encode(x, encoder)?;
                Ok(())
            }
            IdentitySignature::V1(x) => {
                <u32 as Encode>::encode(&(1_u32), encoder)?;
                <[u8; 64] as Encode>::encode(x, encoder)?;
                Ok(())
            }
        }
    }
}

impl Decode for IdentitySignature {
    fn decode<D: Decoder>(decoder: &mut D) -> Result<Self, DecodeError> {
        let variant_index = <u32 as Decode>::decode(decoder)?;
        match variant_index {
            0u32 => {
                let inner = <[u8; 16] as Decode>::decode(decoder)?;
                Ok(Self::V0(Redacted::new(inner)))
            }
            1u32 => {
                let inner = <[u8; 64] as Decode>::decode(decoder)?;
                Ok(Self::V1(Redacted::new(inner)))
            }
            variant => Err(DecodeError::UnexpectedVariant {
                found: variant,
                type_name: "IdentitySignature",
                allowed: AllowedEnumVariants::Range { min: 0, max: 1 },
            }),
        }
    }
}

impl Encode for IdentityNoise {
    fn encode<E: Encoder>(&self, encoder: &mut E) -> Result<(), EncodeError> {
        match self {
            IdentityNoise::V0 => {
                <u32 as Encode>::encode(&(0_u32), encoder)?;
                Ok(())
            }
            IdentityNoise::V1(x) => {
                <u32 as Encode>::encode(&(1_u32), encoder)?;
                <[u8; 16] as Encode>::encode(x, encoder)?;
                Ok(())
            }
        }
    }
}

impl Decode for IdentityNoise {
    fn decode<D: Decoder>(decoder: &mut D) -> Result<Self, DecodeError> {
        let variant_index = <u32 as Decode>::decode(decoder)?;
        match variant_index {
            0u32 => Ok(Self::V0),
            1u32 => {
                let inner = <[u8; 16] as Decode>::decode(decoder)?;
                Ok(Self::V1(Redacted::new(inner)))
            }
            variant => Err(DecodeError::UnexpectedVariant {
                found: variant,
                type_name: "IdentityNoise",
                allowed: AllowedEnumVariants::Range { min: 0, max: 1 },
            }),
        }
    }
}
