use std::{collections::HashMap, sync::Arc};

use actm::traits::Event;
use async_std::sync::RwLock;
use flume::Sender;
use futures::future::join_all;

use crate::types::crypto::Identity;

/// Handle type for connecting nodes to the simulated networking
pub struct ChannelNetworkHandle<E> {
    /// Mapping from a node's identity to its flume channel inbox
    inboxes: Arc<RwLock<HashMap<Identity, Sender<E>>>>,
}

impl<E> ChannelNetworkHandle<E> {
    /// Creates a new, empty, `ChannelNetworkHandle`.
    ///
    /// This handle can be cloned to create more handles pointing to the same network
    #[allow(clippy::new_without_default)]
    pub fn new() -> Self {
        Self {
            inboxes: Arc::new(RwLock::new(HashMap::new())),
        }
    }

    /// Returns true if this `ChannelNetworkHandle` contains a sender for a given identity
    pub async fn contains(&self, identity: &Identity) -> bool {
        let inboxes = self.inboxes.read().await;
        inboxes.contains_key(identity)
    }

    /// Gets the sender for an identity, if there is one
    pub async fn get(&self, identity: &Identity) -> Option<Sender<E>> {
        let inboxes = self.inboxes.read().await;
        inboxes.get(identity).cloned()
    }

    /// Inserts a pair into the internal map
    pub async fn insert(&self, identity: Identity, sender: Sender<E>) {
        let mut inboxes = self.inboxes.write().await;
        inboxes.insert(identity, sender);
    }
}

impl<E: Event> ChannelNetworkHandle<E> {
    /// Clones and sends down all channels
    pub async fn send_all(&self, event: E) {
        let inboxes = self.inboxes.read().await;
        // We'll ignore the errors here
        let _results = join_all(
            inboxes
                .iter()
                .map(|(_, v)| v.send_async(event.stateless_clone())),
        )
        .await;
    }
}

impl<E> Clone for ChannelNetworkHandle<E> {
    fn clone(&self) -> Self {
        Self {
            inboxes: self.inboxes.clone(),
        }
    }
}
