use std::{fmt::Debug, hash::Hash};

use actm::{
    traits::{Actor, Event, EventConsumer, EventProducer},
    types::CompletionToken,
    util::{BasicActor, BasicActorError, WrappedReceiver},
};
use async_trait::async_trait;
use bincode::{Decode, Encode};
use futures::FutureExt;
use snafu::Snafu;
use tracing::{debug, debug_span, error, info, instrument, trace, warn, Instrument};

use crate::types::{
    crypto::Identity,
    network::{
        inbound::{Connect, InboundNetworkEvent, InboundNetworkEventType},
        outbound::{
            Connected, ErrorOccured, MessageReceived, OutboundNetworkEvent,
            OutboundNetworkEventType,
        },
    },
};

/// Handle type for connecting a node to the simulated network
mod handle;

pub use handle::ChannelNetworkHandle;

/// Errors that can happen over a channel network
#[derive(Snafu, Clone, PartialEq, Eq, PartialOrd, Ord, Encode, Decode, Hash, Debug)]
pub enum ChannelNetworkingError {
    /// Attempted to connect to an identity that didn't exist
    #[snafu(display(
        "Attempted to connect to an identity that didn't exist: {:?}",
        identity
    ))]
    MissingIdentity {
        /// The identity that was missing
        identity: Identity,
    },
    /// Failed to configure the internal actor
    ActorConfiguration,
}

/// Outbound event type
type OutboundEvent<T> = OutboundNetworkEvent<T, Identity, ChannelNetworkingError>;
/// Inbound event type
type InboundEvent<T> = InboundNetworkEvent<T, Identity>;

/// Connection to an in-memory networking simulation
#[derive(Clone)]
pub struct ChannelNetworking<T>
where
    T: Event + Clone, // FIXME: Drop this clone bound, will take a bit of work
{
    /**
    Internal [`BasicActor`]
    */
    actor: BasicActor<InboundEvent<T>, OutboundEvent<T>>,
}

impl<T> ChannelNetworking<T>
where
    T: Event + Clone, // FIXME: Drop this clone bound, will take a bit of work
{
    /// Creates a new `ChannelNetworking` from the given handle
    ///
    /// The optional `bound` argument will be used to set the maximum size of the buffers internal
    /// channels, unbounded channles will be used if this argument is `None`
    #[instrument(skip(handle))]
    pub async fn new(
        handle: ChannelNetworkHandle<InboundEvent<T>>,
        identity: Identity,
        bound: Option<usize>,
    ) -> Result<Self, ChannelNetworkingError> {
        // Go ahead and create our channel pair
        let (tx, mut receiver) = WrappedReceiver::new(bound);
        // Insert the tx side into our handle
        handle.insert(identity, tx).await;
        // Send a connection message to everyone already on the network
        let connection_event: InboundEvent<T> = InboundNetworkEventType::Connect(Connect {
            address: identity,
            identity: Some(identity),
        })
        .into();
        handle.send_all(connection_event).await;
        // Passthrough to the spawn_async method on the implementing BasicActor, using our
        // `ChannelNetworkHandle` as the context
        info!("Spawning new channel networking backing actor");
        let actor: BasicActor<InboundEvent<T>, OutboundEvent<T>> = BasicActor::spawn_async(
            move |handle: &mut ChannelNetworkHandle<InboundEvent<T>>,
                  mut event: InboundEvent<T>| {
                trace!(?event, "Handling event");
                async move {
                    // Get the token if we have one
                    let token = event.token();
                    // Match on the inbound event
                    match event.into_inner() {
                        InboundNetworkEventType::Connect(connect) => {
                            debug!(?connect, "Handling connect message");
                            let identity = connect.address;
                            // First, build up the reply event
                            let mut event: OutboundNetworkEvent<
                                T,
                                Identity,
                                ChannelNetworkingError,
                            > = OutboundNetworkEventType::Connected(Connected {
                                address: identity,
                                identity,
                            })
                            .into();
                            // Check to see if we are responding to a request
                            //
                            // This is a stub behavior, we are only validating if the connection
                            // already exists
                            if let Some(token) = token {
                                // Since this is a request, we'll need to throw an error if we don't
                                // have it
                                if handle.contains(&identity).await {
                                    // Attach the token and send it out
                                    event.set_completion_token(token);
                                    Some(event)
                                } else {
                                    // Oh no! An error
                                    error!(
                                        ?identity,
                                        "Attempted to connect to identity not in map"
                                    );
                                    // Build up the error
                                    let error = MissingIdentitySnafu { identity }.build();
                                    // The event
                                    let mut event: OutboundNetworkEvent<
                                        T,
                                        Identity,
                                        ChannelNetworkingError,
                                    > = OutboundNetworkEventType::ErrorOccured(ErrorOccured {
                                        error,
                                        response: true,
                                    })
                                    .into();
                                    // Attach the token
                                    event.set_completion_token(token);
                                    // Send it out
                                    Some(event)
                                }
                            } else {
                                // Since this isn't a request, just throw it out, so long as we have
                                // it
                                if handle.contains(&identity).await {
                                    Some(event)
                                } else {
                                    warn!(?identity, "Non-request connect from missing identity");
                                    None
                                }
                            }
                        }
                        InboundNetworkEventType::DirectMessage(message) => {
                            debug!(?message, "Handling direct message event");
                            // See if the message was meant for us
                            if message.identity == identity {
                                trace!(?identity, ?message, "Message was for us");
                                // Our message, send it out
                                // FIXME: This should return the senders identity, not ours
                                let event = OutboundNetworkEventType::Message(MessageReceived {
                                    message: message.message,
                                    identity,
                                    direct: true,
                                })
                                .into();
                                Some(event)
                            } else {
                                trace!(?identity, ?message, "Message was not for us");
                                let identity = message.identity;
                                // Not for us, pass it along
                                if let Some(sender) = handle.get(&identity).await {
                                    let event =
                                        InboundNetworkEventType::DirectMessage(message).into();
                                    // Explicitly ignore error
                                    if let Err(e) = sender.send_async(event).await {
                                        error!(?e, "Failed to send message");
                                    }
                                    None
                                } else {
                                    // Oh no! An error
                                    error!(
                                        ?identity,
                                        "Attempted to send message to identity not in map"
                                    );
                                    // Build up the error
                                    let error = MissingIdentitySnafu { identity }.build();
                                    // The event
                                    let mut event: OutboundNetworkEvent<
                                        T,
                                        Identity,
                                        ChannelNetworkingError,
                                    > = OutboundNetworkEventType::ErrorOccured(ErrorOccured {
                                        error,
                                        response: true,
                                    })
                                    .into();
                                    // Attach the token
                                    // TODO: Is this unnessicary? it should be
                                    if let Some(token) = token {
                                        event.set_completion_token(token);
                                    };
                                    // Send it out
                                    Some(event)
                                }
                            }
                        }
                        InboundNetworkEventType::Disconnect(d) => {
                            // This is... abnormal
                            warn!(?d, "Got a disconnect in ChannelNetwork");
                            None
                        }
                    }
                }
                .instrument(debug_span!("Channel Networking Inner"))
                .boxed()
            },
            handle,
            bound,
        );
        // connect ourselves up to our receiver
        receiver
            .register_consumer(actor.inbox().await)
            .await
            .map_err(|_| ActorConfigurationSnafu.build())?;
        Ok(Self { actor })
    }
}

// Trait passthroughs
// TODO Make a macro to generate type safety wrappers for BasicActor

#[async_trait]
impl<T> Actor<InboundEvent<T>, OutboundEvent<T>> for ChannelNetworking<T>
where
    T: Event + Clone, // FIXME: Drop this clone bound, will take a bit of work
{
    type Inbox = Self;

    type Outbox = Self;

    async fn inbox(&self) -> Self::Inbox {
        self.clone()
    }

    async fn outbox(&self) -> Self::Outbox {
        self.clone()
    }
}

#[async_trait]
impl<T> EventConsumer<InboundEvent<T>> for ChannelNetworking<T>
where
    T: Event + Clone, // FIXME: Drop this clone bound, will take a bit of work
{
    type Error = BasicActorError;

    async fn accept(&mut self, event: InboundEvent<T>) -> Result<(), Self::Error> {
        self.actor.accept(event).await
    }
}

#[async_trait]
impl<T> EventProducer<OutboundEvent<T>> for ChannelNetworking<T>
where
    T: Event + Clone, // FIXME: Drop this clone bound, will take a bit of work
{
    type Error = BasicActorError;

    async fn register_consumer<C>(&mut self, consumer: C) -> Result<(), Self::Error>
    where
        C: EventConsumer<OutboundEvent<T>> + Send + Sync + 'static,
    {
        self.actor.register_consumer(consumer).await
    }

    async fn register_priority_consumer<C>(&mut self, consumer: C) -> Result<(), Self::Error>
    where
        C: EventConsumer<OutboundEvent<T>> + Send + Sync + 'static,
    {
        self.actor.register_priority_consumer(consumer).await
    }
    async fn register_callback<F>(
        &mut self,
        callback: F,
        token: CompletionToken,
    ) -> Result<(), Self::Error>
    where
        F: FnOnce(OutboundEvent<T>) + Send + Sync + 'static,
    {
        self.actor.register_callback(callback, token).await
    }
}

#[cfg(test)]
mod tests {

    use async_std::task::spawn;
    use tracing_subscriber::EnvFilter;

    use super::*;
    use crate::types::{crypto::Protocol, network::inbound::DirectMessage};

    #[derive(Clone, PartialEq, Eq, Debug, PartialOrd, Ord, Hash, Encode, Decode)]
    struct Message {
        content: String,
    }
    impl Event for Message {
        type Flags = ();

        fn flags(&self) -> Self::Flags {}

        fn stateless_clone(&self) -> Self {
            self.clone()
        }
    }

    // Utility function
    fn true_boi<A, B>(_a: &A, _b: &B) -> bool {
        true
    }

    // Basic smoke test, make sure that two nodes can communicate
    #[async_std::test]
    async fn smoke() {
        tracing_subscriber::fmt()
            .pretty()
            .with_env_filter(EnvFilter::from_default_env())
            .init();
        // Use maximum backpressure to make things behave more deterministically
        let bound = Some(1);
        // Get our identities
        let alice_secret = Protocol::V0.generate_identity_from_seed([0_u8]);
        let alice_public = alice_secret.identity();
        let bob_secret = Protocol::V0.generate_identity_from_seed([1_u8]);
        let bob_public = bob_secret.identity();
        // Spin up a network handle
        let handle = ChannelNetworkHandle::new();
        // Spin up our actors
        let mut alice = ChannelNetworking::<Message>::new(handle.clone(), alice_public, bound)
            .await
            .unwrap();
        let mut bob = ChannelNetworking::<Message>::new(handle.clone(), bob_public, bound)
            .await
            .unwrap();
        // Spawn some communication channels
        let (alice_tx, alice_rx) = flume::bounded(1);
        let (bob_tx, bob_rx) = flume::bounded(1);
        // Register our channels to our actors
        alice
            .register_consumer((true_boi, move |e| {
                println!("Processsing Alice");
                if let Err(e) = alice_tx.send(e) {
                    println!("Error processing Alice: {:?}", e);
                }
                println!("Processed Alice");
            }))
            .await
            .unwrap();
        bob.register_consumer((true_boi, move |e| {
            println!("Processsing Bob");
            if let Err(e) = bob_tx.send(e) {
                println!("Error processing bob: {:?}", e);
            }
            println!("Processed Bob");
        }))
        .await
        .unwrap();
        // Have bob send a message to alice
        println!("Messaging alice");
        bob.accept(InboundNetworkEvent::from(
            InboundNetworkEventType::DirectMessage(DirectMessage {
                message: Message {
                    content: "Hello Alice!".to_string(),
                },
                identity: alice_public,
            }),
        ))
        .await
        .unwrap();
        // yield to the executor
        println!("Messaging Bob");
        // Have alice send a message to bob
        alice
            .accept(InboundNetworkEvent::from(
                InboundNetworkEventType::DirectMessage(DirectMessage {
                    message: Message {
                        content: "Hello Bob!".to_string(),
                    },
                    identity: bob_public,
                }),
            ))
            .await
            .unwrap();
        println!("Sent messages");
        // Process the outboxes
        //
        // This gets a little fucky since we have to make sure everything's driven to completion
        println!("Getting alice's messages");
        let mut alice_message = alice_rx.recv_async().await.unwrap().into_inner();
        while matches!(alice_message, OutboundNetworkEventType::Connected(_)) {
            alice_message = alice_rx.recv_async().await.unwrap().into_inner();
        }
        // Spawn off a background task to pull any more messages out of alice
        spawn(async move {
            loop {
                let res = alice_rx.recv_async().await;
                println!("Extra message from alice: {:?}", res);
            }
        });
        println!("alice: {:?}", alice_message);
        println!("Getting bobs's messages");
        let mut bob_message = bob_rx.recv_async().await.unwrap().into_inner();
        while matches!(bob_message, OutboundNetworkEventType::Connected(_)) {
            bob_message = bob_rx.recv_async().await.unwrap().into_inner();
        }
        // Spawn off a background task to pull any more messages out of alice
        spawn(async move {
            loop {
                let res = bob_rx.recv_async().await;
                println!("Extra message from bob: {:?}", res);
            }
        });
        println!("bob: {:?}", bob_message);

        // Verify that our messages have the right contents
        match alice_message {
            OutboundNetworkEventType::Message(MessageReceived { message, .. }) => {
                assert_eq!(&message.content, "Hello Alice!");
            }
            _ => {
                panic!("Invalid message type");
            }
        }
        match bob_message {
            OutboundNetworkEventType::Message(MessageReceived { message, .. }) => {
                assert_eq!(&message.content, "Hello Bob!");
            }
            _ => {
                panic!("Invalid message type");
            }
        }
    }
}
