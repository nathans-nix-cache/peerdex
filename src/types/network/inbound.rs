use actm::{traits::Event, util::WrappedEvent};
use bincode::{Decode, Encode};
use enum_dispatch::enum_dispatch;

use crate::types::crypto::Identity;

/// Trait for inbound (being received by the networking [`Actor`](actm::traits::Actor))
/// networking [`Event`]'s
#[enum_dispatch]
pub trait InboundNetwork<T: Event> {
    /// Returns the payload by reference, if there is one
    fn payload(&self) -> Option<&T>;
    /// Returns true if there is a payload
    fn has_payload(&self) -> bool {
        self.payload().is_some()
    }
}

/// Dial up the node at the provided address
#[derive(Clone, PartialEq, Eq, Debug, PartialOrd, Ord, Hash, Encode, Decode)]
pub struct Connect<A> {
    /// Address to connect to
    pub address: A,
    /// Identity expected to be at that address, if an expectation is had
    pub identity: Option<Identity>,
}

impl<T: Event, A> InboundNetwork<T> for Connect<A> {
    fn payload(&self) -> Option<&T> {
        None
    }
}

/// Send a message to the node at the provided address
#[derive(Clone, PartialEq, Eq, Debug, PartialOrd, Ord, Hash, Encode, Decode)]
pub struct DirectMessage<T: Event> {
    /// Message to send
    pub message: T,
    /// Identity to send to
    pub identity: Identity,
}

impl<T: Event> InboundNetwork<T> for DirectMessage<T> {
    fn payload(&self) -> Option<&T> {
        Some(&self.message)
    }
}

/// Disconnect from a given node
#[derive(Clone, PartialEq, Eq, Debug, PartialOrd, Ord, Hash, Encode, Decode)]
pub struct Disconnect {
    /// Identity of the node to disconnect from
    pub identity: Option<Identity>,
}

impl<T: Event> InboundNetwork<T> for Disconnect {
    fn payload(&self) -> Option<&T> {
        None
    }
}

/// Enumeration for inbound (being received by the networking [`Actor`](actm::traits::Actor))
/// networking [`Event`]'s
///
/// Type Alias Definitions:
///
/// | `T` | The [`Event`] type       |
/// | `A` | The network address type |
#[enum_dispatch(InboundNetwork)]
#[derive(Clone, PartialEq, Eq, Debug, PartialOrd, Ord, Hash, Encode, Decode)]
pub enum InboundNetworkEventType<T: Event, A> {
    /// A [`Connect`] event
    Connect(Connect<A>),
    /// A [`DirectMessage`] event
    DirectMessage(DirectMessage<T>),
    /// A [`Disconnect`] event
    Disconnect,
}

/// [`Event`] type for network events in the networking [`Actor`](actm::traits::Actor)'s inbox
pub type InboundNetworkEvent<T, A> = WrappedEvent<InboundNetworkEventType<T, A>>;
