use std::error::Error;

use actm::{traits::Event, util::WrappedEvent};
use bincode::{Decode, Encode};
use enum_dispatch::enum_dispatch;

use crate::types::crypto::Identity;

/// Trait for outbound (being sent by the networking [`Actor`](actm::traits::Actor))
/// networking [`Event`]'s
pub trait OutboundNetwork<T: Event> {
    /// Returns the payload by reference, if there is one
    fn payload(&self) -> Option<&T>;
    /// Returns true if there is a payload
    fn has_payload(&self) -> bool {
        self.payload().is_some()
    }
}

/// Node at the contained address with the contained identity has connected to the local node
///
/// This may not be a response to an incoming [`Connect`](super::inbound::Connect) event, this will
/// also fire if a remote node connects spontaneously
#[derive(Clone, PartialEq, Eq, Debug, PartialOrd, Ord, Hash, Encode, Decode)]
pub struct Connected<A> {
    /// Address of the connecting node
    pub address: A,
    /// Identity of the remote node
    pub identity: Identity,
}

impl<A, T: Event> OutboundNetwork<T> for Connected<A> {
    fn payload(&self) -> Option<&T> {
        None
    }
}

/// Received a message
#[derive(Clone, PartialEq, Eq, Debug, PartialOrd, Ord, Hash, Encode, Decode)]
pub struct MessageReceived<T: Event> {
    /// The received message
    pub message: T,
    /// The identity the message came from
    pub identity: Identity,
    /// Was this a direct message
    pub direct: bool,
}

impl<T: Event> OutboundNetwork<T> for MessageReceived<T> {
    fn payload(&self) -> Option<&T> {
        Some(&self.message)
    }
}

/// Remote node disconnected from local node
///
/// This may not be a response to an incoming [`Connect`](super::inbound::Connect) event, this will
/// also fire if a remote node disconnects spontaneously or times out
#[derive(Clone, PartialEq, Eq, Debug, PartialOrd, Ord, Hash, Encode, Decode)]
pub struct Disconnected {
    /// The identity of the disconnected node
    pub identity: Identity,
}

impl<T: Event> OutboundNetwork<T> for Disconnected {
    fn payload(&self) -> Option<&T> {
        None
    }
}

/// An error occurred
///
/// This may either be in response to an event, or spontaneous
#[derive(Clone, PartialEq, Eq, Debug, PartialOrd, Ord, Hash, Encode, Decode)]
pub struct ErrorOccured<B: Error> {
    /// The error
    pub error: B,
    /// Was this error in response to a request?
    pub response: bool,
}

impl<T: Event, B: Error> OutboundNetwork<T> for ErrorOccured<B> {
    fn payload(&self) -> Option<&T> {
        None
    }
}

/// Enumeration for outbound (being sent by the networking [`Actor`](actm::traits::Actor))
/// networking [`Event`]'s
#[enum_dispatch(OutboundNetwork)]
#[derive(Clone, PartialEq, Eq, Debug, PartialOrd, Ord, Hash, Encode, Decode)]
pub enum OutboundNetworkEventType<T: Event, A, B: Error> {
    /// A [`Connected`] message
    Connected(Connected<A>),
    /// A [`MessageReceived`] message
    Message(MessageReceived<T>),
    /// A [`Disconnected`] message
    Disconnected,
    /// An [`ErrorOccured`] message
    ErrorOccured(ErrorOccured<B>),
}

/// [`Event`] type for network events being sent out by the networking
/// [`Actor`](actm::traits::Actor)
pub type OutboundNetworkEvent<T, A, B> = WrappedEvent<OutboundNetworkEventType<T, A, B>>;
