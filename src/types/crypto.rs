//! Definition of the cryptographic protocol for `PeerDex`
use std::fmt::Debug;

use bincode::{Decode, Encode};
#[cfg(test)]
use proptest_derive::Arbitrary;
use redacted::Redacted;
use xxhash_rust::xxh3;

mod bincode_impls;
mod error;
mod hash;
mod identity;

pub use error::CryptographyError;
use error::UnsafeProtocolSnafu;
pub use hash::Hash;
pub use identity::{Identity, IdentityNoise, IdentitySecret, IdentitySignature};

/// Representation of the version of the protocol in use
#[derive(Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, Debug, Encode, Decode)]
#[cfg_attr(test, derive(Arbitrary))]
pub enum Protocol {
    /// Version 0 of the protocol, using the following primatives:
    ///
    /// | Hash                | [XXH3](https://github.com/Cyan4973/xxHash) |
    /// | Identity Signatures | Keyed XXH3                                 |
    ///
    /// # <span style="color:red">**Warning**</span>
    ///
    /// This version of the protocol uses insecure non-cryptographic operations, it is provided for
    /// testing purposes only, _do not_ use in production.
    V0,
    /// Version 1 of the protocol, using the following primatives:
    ///
    /// | Hash                | [BLAKE3](https://github.com/BLAKE3-team/BLAKE3)             |
    /// | Identity Signatures | [ed25519](https://github.com/jedisct1/rust-ed25519-compact) |
    V1,
}

impl Protocol {
    /// Hashes the given byte string and returns the appropiate variant of the [`enum@Hash`] enum for
    /// this version of the protocol
    pub fn hash(&self, bytes: impl AsRef<[u8]>) -> Hash {
        match self {
            Protocol::V1 => Hash::V1(Redacted::new(*blake3::hash(bytes.as_ref()).as_bytes())),
            Protocol::V0 => Hash::V0(Redacted::new(xxh3::xxh3_128(bytes.as_ref()).to_le_bytes())),
        }
    }

    /// Generates a new, "random", [`IdentitySecret`] from the given seed data
    pub fn generate_identity_from_seed(&self, seed: impl AsRef<[u8]>) -> IdentitySecret {
        match self {
            Protocol::V0 => {
                // Simply hash the seed data and repeat it
                let mut buffer = [0_u8; 136];
                let hash = xxh3::xxh3_128(seed.as_ref()).to_le_bytes();
                for (i, x) in hash.repeat(9).into_iter().take(136).enumerate() {
                    buffer[i] = x;
                }
                // That's it, thats our "secret"
                IdentitySecret::V0(Redacted::new(buffer))
            }
            Protocol::V1 => {
                // First hash with blake3 to get a 32 byte seed
                let seed: [u8; 32] = *blake3::hash(seed.as_ref()).as_bytes();
                // Then pump it into ed25519_compat
                let seed = ed25519_compact::Seed::new(seed);
                // Generate our key pair
                let kp = ed25519_compact::KeyPair::from_seed(seed);
                // turn it into bytes
                let key_bytes: [u8; 64] = *kp;
                IdentitySecret::V1(Redacted::new(key_bytes))
            }
        }
    }

    /// Generate a new, "random", [`IdentityNoise`] from the given seed data
    pub fn generate_identity_noise_from_seed(&self, seed: impl AsRef<[u8]>) -> IdentityNoise {
        match self {
            // V0 protocol does not support noise
            Protocol::V0 => IdentityNoise::V0,
            // The v1 protocol uses 16 byte noise, hash our input and grab the first 16 bytes off
            Protocol::V1 => {
                let seed: [u8; 32] = *blake3::hash(seed.as_ref()).as_bytes();
                // trim it
                let seed: [u8; 16] = seed[0..16].try_into().expect("Impossible size mismatch");
                IdentityNoise::V1(Redacted::new(seed))
            }
        }
    }

    /// Returns an error if the version of the protocol is either an unsafe development only
    /// protocol, such as `Protocol::V0`, or has known vulnerabilites that make it unsafe to
    /// interact with.
    ///
    /// This method is intended for production clients to assert.
    ///
    /// ```
    /// use peerdex::types::crypto::Protocol;
    ///
    /// assert!(Protocol::V0.safe().is_err());
    /// assert!(Protocol::V1.safe().is_ok());
    /// ```
    ///
    /// # Errors
    ///
    /// Returns `Err(CryptographyError::UnsafeProtocol)` if the protocol is unsafe
    pub fn safe(&self) -> Result<(), CryptographyError> {
        match self {
            Protocol::V0 => UnsafeProtocolSnafu {
                protocol: Protocol::V0,
            }
            .fail(),
            Protocol::V1 => Ok(()),
        }
    }
}

/// Indicates that the implementor is part of the cryptographic protocol
pub trait HasProtocol:
    Clone + Eq + Ord + std::hash::Hash + Debug + Encode + Decode + Send + Sync + 'static
{
    /// Provides the associated [`Protocol`] version
    fn protocol(&self) -> Protocol;
}
