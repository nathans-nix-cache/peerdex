//! Abstractions over a networking backend

use std::{error::Error, fmt::Debug, hash::Hash};

/// Implementations of this trait
pub mod impls;
/// Inbound [`Event`](actm::traits::Event) types
pub mod inbound;
/// Outbound [`Event`](actm::traits::Event) types
pub mod outbound;

use actm::traits::{Actor, Event};
use bincode::{Decode, Encode};
use inbound::InboundNetworkEvent;
use outbound::OutboundNetworkEvent;

/// A `Networking` is an [`Actor`] that acts on the networking event types
pub trait Networking<T, A, B>:
    Actor<InboundNetworkEvent<T, A>, OutboundNetworkEvent<T, A, B>>
where
    T: Event + Clone, // FIXME: Drop this clone bound, will take a bit of work
    A: Clone + Ord + Encode + Decode + Hash + Debug + Send + Sync + 'static,
    B: Error + Clone + Ord + Encode + Decode + Hash + Debug + Send + Sync + 'static,
{
}
