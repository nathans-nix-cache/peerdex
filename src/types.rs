//! Contains common types and traits used by `PeerDex`

pub mod crypto;
pub mod network;

/// Configuration trait that defines all the generic types on an instance of an Indexer
///
/// This trait allows us to collect the the traits abstracting multiple individual behaviors into
/// one type, avoiding having large numbers of type arguments and a lot of instances of annoying
/// type pathing.
///
/// It is reccomended to implement this trait on a zero sized type, see example below, as this type is never actually
/// consumed by the library:
///
/// ``` ignore
/// struct MyStruct;
/// impl IndexerImplementation for MyStruct {
///     ...
/// }
/// ```
pub trait IndexerImplementation {}
