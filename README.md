# PeerDex

Peer to peer search engine, currently an extreme work in progress.

This project is licensed under the terms of the [the AGPL 3.0 or later](./LICENSE.md).

This crate makes heavy use of the of the [`actm`](https://git.sr.ht/~thatonelutenist/actm) async
actors framework, which is developed in tandem with this crate.

## Contributing

Please see [contributing.md](./doc/contributing.md) for information and instructions on contributing
to PeerDex. Please also see our project hub at
[~thatonelutenist/PeerDex](https://sr.ht/~thatonelutenist/PeerDex/).
